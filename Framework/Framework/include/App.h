#pragma once

#include <Windows.h>
#include <cstdint>
#include <d3d12.h>
#include <dxgi1_4.h>
#include <ComPtr.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <DescriptorPool.h>
#include <ColorTarget.h>
#include <DepthTarget.h>
#include <CommandList.h>
#include <Fence.h>
#include <Mesh.h>
#include <Texture.h>
#include <InlineUtil.h>

#pragma comment( lib, "d3d12.lib" )  
#pragma comment( lib, "dxgi.lib" )   
#pragma comment( lib, "d3dcompiler.lib" )
#pragma comment( lib, "dxguid.lib" )

class App
{
public:
    App(const uint32_t Width, const uint32_t Height);
    ~App();

    void Run();

protected:
	enum POOL_TYPE
	{
		POOL_TYPE_RES = 0,
		POOL_TYPE_SMP, // sampler
		POOL_TYPE_RTV,
		POOL_TYPE_DSV,
		POOL_COUNT,
	};

	// FrameCount
	static const uint32_t FrameCount = 2;

    // regard window parameter
    HINSTANCE HInstance = nullptr;
    HWND HWnd = nullptr;
    uint32_t Width = 960;
    uint32_t Height = 540;	

	// d3d12
	ComPtr<ID3D12Device> D3D12Device = nullptr;
	ComPtr<ID3D12CommandQueue> CommandQueue = nullptr;
	ComPtr<IDXGISwapChain3> SwapChain = nullptr;
	ColorTarget ColorTarget_[FrameCount] = {};
	DepthTarget DepthTarget_ = {};
	DescriptorPool* pPool[POOL_COUNT];
	CommandList CommandList_ = {};
	Fence Fencd_ = {};
	uint32_t FrameIndex = 0;
	D3D12_VIEWPORT Viewport = {};
	D3D12_RECT Scissor = {};

	void Present(uint32_t Interval);

	virtual bool OnInitialize()
	{
		return true;
	}

	virtual bool OnTerminate()
	{}

	virtual void OnRender()
	{}

	virtual void OnMsgProc(HWND, UINT, WPARAM, LPARAM)
	{}

private:
    const bool InitializeApp();
    void TerminateApp();
    const bool InitializeWindow();
    void TerminateWindow();
    void MainLoop();
    bool InitializeD3D();
    void TerminateD3D();

    static LRESULT CALLBACK WndProc(HWND HWnd, UINT Msg, WPARAM Wp, LPARAM Lp);
};

