#pragma once

#include <d3d12.h>
#include <dxgi1_6.h>
#include <ComPtr.h>
#include <cstdint>

class DescriptorHandle;
class DescriptorPool;

class ColorTarget
{
public:
	// Ctr & Dtr
	ColorTarget();
	~ColorTarget();

	// Initialize
	const bool Initialize(
		ID3D12Device* pDevice,
		DescriptorPool* pRTVPool,
		uint32_t Width,
		uint32_t Height,
		DXGI_FORMAT Format_
	);

	const bool InitializeFromBackBuffer(
		ID3D12Device* pDevice,
		DescriptorPool* pRTVPool,
		uint32_t BackBufferNumber,
		IDXGISwapChain* pSwapChain
	);

	// Terminate
	void Terminate();

	// Return RTVHandle
	DescriptorHandle* GetRTVHandle() const;

	// Return Resource
	ID3D12Resource* GetResource() const;

	// Return Desc
	D3D12_RESOURCE_DESC GetDesc() const;

	// Return ViewDesc
	D3D12_RENDER_TARGET_VIEW_DESC GetViewDesc() const;

private:
	ColorTarget(const ColorTarget&) = delete;
	void operator=(const ColorTarget&) = delete;

	ComPtr<ID3D12Resource> pTarget = nullptr;
	DescriptorHandle* pRTVHandle = nullptr;
	DescriptorPool* pRTVPool = nullptr;
	D3D12_RENDER_TARGET_VIEW_DESC ViewDesc = {};
};