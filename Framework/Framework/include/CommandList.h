#pragma once

#include <d3d12.h>
#include <ComPtr.h>
#include <cstdint>
#include <vector>

class CommandList
{
public:
	// Ctr & Dtr
	CommandList();
	~CommandList();

	// Initialize
	const bool Initialize(ID3D12Device* pDevice, D3D12_COMMAND_LIST_TYPE Type, uint32_t Count);

	// Terminate
	void Terminate();

	// Return ResetCommandList
	ID3D12GraphicsCommandList* Reset();

private:
	CommandList(const CommandList&)  = delete;
	void operator=(const CommandList&) = delete;

	ComPtr<ID3D12GraphicsCommandList> pCmdList = nullptr;
	std::vector<ComPtr<ID3D12CommandAllocator>> pAllocators = {};
	uint32_t AllocatorNumber = 0;
};