#pragma once

#include <d3d12.h>
#include <ComPtr.h>
#include <vector>

class DescriptorHandle;
class DescriptorPool;

class ConstantBuffer
{
public:
	ConstantBuffer();

	~ConstantBuffer();

	// Initialize
	const bool Initialize(ID3D12Device* pDevice, DescriptorPool* pPool, size_t Size);

	// Finalize
	void Terminate();

	// Return VirtulaAddress
	D3D12_GPU_VIRTUAL_ADDRESS GetAddress() const;

	// Return CPUHandle
	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUHandle() const;

	// Return GPUHandle
	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUHandle() const;

	// Return MappedPointer
	void* GetPtr() const;

	template<typename T>
	T* GetPtr()
	{
		return reinterpret_cast<T*>(GetPtr());
	}

private:
	ConstantBuffer(const ConstantBuffer&) = delete;
	void operator=(const ConstantBuffer&) = delete;

	ComPtr<ID3D12Resource> pConstantBuffer = nullptr;
	DescriptorHandle* pHandle = nullptr;
	DescriptorPool* pPool = nullptr;
	D3D12_CONSTANT_BUFFER_VIEW_DESC CBDesc = {};
	void* MappedPtr = nullptr;
};