#pragma once

#include <d3d12.h>
#include <ComPtr.h>
#include <cstdint>

class DescriptorHandle;
class DescriptorPool;

class DepthTarget
{
public:
	// Ctr & Dtr
	DepthTarget();
	~DepthTarget();

	// Initialize
	const bool Initialize(
		ID3D12Device* pDevice,
		DescriptorPool* pDSVPool,
		uint32_t Width,
		uint32_t Height, 
		DXGI_FORMAT Format_
	);

	// Finalize
	void Terminate();

	// Return DSVHandle
	DescriptorHandle* GetDSVHandle() const;

	// Return Resource
	ID3D12Resource* GetResource() const;

	// Return RscDesc
	D3D12_RESOURCE_DESC GetDesc() const;

	// Return DepthStencilDesc
	D3D12_DEPTH_STENCIL_VIEW_DESC GetViewDesc() const;

private:
	DepthTarget(const DepthTarget&) = delete;
	void operator=(const DepthTarget&) = delete;

	ComPtr<ID3D12Resource> pTarget = nullptr;
	DescriptorHandle* pDSVHandle = nullptr;
	DescriptorPool* pDSVPool = nullptr;
	D3D12_DEPTH_STENCIL_VIEW_DESC ViewDesc = {};
};