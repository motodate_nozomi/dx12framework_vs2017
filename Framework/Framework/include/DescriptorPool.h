#pragma once

#include <d3d12.h>
#include <atomic>
#include <ComPtr.h>
#include <Pool.h>

// Descriptor Handle
class DescriptorHandle
{
public:
	const bool HasCPUHandle() const {
		return CPUHandle.ptr != 0;
	}

	const bool HasGPUHandle() const {
		return GPUHandle.ptr != 0;
	}

	D3D12_CPU_DESCRIPTOR_HANDLE CPUHandle = {};
	D3D12_GPU_DESCRIPTOR_HANDLE GPUHandle = {};
};

class DescriptorPool
{
public:
	// Create
	static const bool Create(ID3D12Device* pDevice, const D3D12_DESCRIPTOR_HEAP_DESC* pDesc, DescriptorPool** ppPool);

	// Add Reference
	void AddReff();

	// Release
	void Release();

	// Return RefCount
	const uint32_t GetRefCount() const;

	// Assign DescriptorHandle
	DescriptorHandle* AllocHandle();

	// Free Handle
	void FreeHandle(DescriptorHandle*& pHandle);

	// Return AvailableHandle
	const uint32_t GetAvailableHandleCount() const;

	// Return AllocatedHandle
	const uint32_t GetHandleNum() const;

	// Return AllHandleNum
	const uint32_t GetHandleCapacity() const;

	// Return DescriptorHeap
	ID3D12DescriptorHeap* const GetHeap() const;

private:
	DescriptorPool();
	~DescriptorPool();

	DescriptorPool(const DescriptorPool&) = delete;
	void operator=(const DescriptorPool&) = delete;

	std::atomic<uint32_t> RefCount = {};
	Pool<DescriptorHandle> HandlePool = {};
	ComPtr<ID3D12DescriptorHeap> pHeap = nullptr;
	uint32_t DescriptorSize = 0;
};