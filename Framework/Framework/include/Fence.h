#pragma once

#include <d3d12.h>
#include <ComPtr.h>

class Fence
{
public:
	// Ctr & Dtr
	Fence();
	~Fence();

	// Initalize
	const bool Initialize(ID3D12Device* pDevice);

	// Finalize
	void Terminate();

	// Wait to specified time
	void Wait(ID3D12CommandQueue* pQueue, UINT Timeout);

	// Wait until state is signal
	void Sync(ID3D12CommandQueue* pQueue);

private:
	Fence(const Fence&) = delete;
	void operator=(const Fence&) = delete;

	ComPtr<ID3D12Fence> pFence = nullptr;
	HANDLE Event_ = {};
	UINT Counter = 0;
};