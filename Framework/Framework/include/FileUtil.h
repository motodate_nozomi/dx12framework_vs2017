#pragma once

#include <string>
#include <Shlwapi.h>

#pragma comment(lib, "shlwapi.lib")

// Check exist file
const bool SearchFilePahtA(const char* FileName, std::string& Result);

const bool SearchFilePathW(const wchar_t* FileName, std::wstring& Result);

// branch use charSet
#if defined(UNICODE) || defined(_UNICODE)
__forceinline const bool SearchFilePath(const wchar_t* FileName, std::wstring& Result)
{
    return SearchFilePathW(FileName, Result);
}
#else
__forceinline const bool SearchFilePath(const wchar_t* FileName, std::wstring& Result)
{
    return SearchFilePahtA(FileName, Result);
}
#endif // defined(UNICODE) || defined(_UNICODE)