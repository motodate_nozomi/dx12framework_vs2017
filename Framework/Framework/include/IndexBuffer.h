#pragma once

#include <d3d12.h>
#include <ComPtr.h>
#include <cstdint>

class IndexBuffer
{
public:
	IndexBuffer();
	~IndexBuffer();

	// Initialize
	const bool Initialize(ID3D12Device* pDevice, const size_t Size, const uint32_t* pInitData = nullptr);

	// Finalize
	void Terminate();

	// Mapping
	uint32_t* Map();

	// Unmapping
	void Unmap();

	// Return IndexBufferView
	D3D12_INDEX_BUFFER_VIEW GetView() const;

private:
	IndexBuffer(const IndexBuffer&) = delete;
	void operator=(const IndexBuffer&) = delete;

	ComPtr<ID3D12Resource> pIndexBuffer = nullptr;
	D3D12_INDEX_BUFFER_VIEW IBView = {};
};