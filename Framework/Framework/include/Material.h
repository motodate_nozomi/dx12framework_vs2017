#pragma once

#include <DescriptorPool.h>
#include <ResourceUploadBatch.h>
#include <Texture.h>
#include <ConstantBuffer.h>
#include <map>

class Material 
{
public:
	enum TEXTURE_USAGE
	{
		TEXTURE_USAGE_DIFFUSE = 0,
		TEXTURE_USAGE_SPECULAR,
		TEXTURE_USAGE_SHININESS,
		TEXTURE_USAGE_NORMAL,

		TEXTURE_USAGE_COUNT,
	};

	// Ctr, Dtr
	Material();
	~Material();

	// Initialize
	const bool Initialize(
		ID3D12Device* pDevice,
		DescriptorPool* pPool,
		size_t BufferSize,
		size_t MaterialCount
	);

	// Finalize
	void Terminate();

	// Set texture
	const bool SetTexture(
		const size_t MaterialNum,
		TEXTURE_USAGE TexUsage,
		const std::wstring& TexPath,
		DirectX::ResourceUploadBatch& Batch
	);

	// Return ConstantBuffer
	void* GetBufferPtr(const size_t MaterialNum) const;

	template<typename T>
	T* GetBufferPtr(const size_t MaterialNum) const 
	{
		return reinterpret_cast<T*>(GetBufferPtr(MaterialNum));
	}

	// Return VirtualAddress for ConstantBuffer
	D3D12_GPU_VIRTUAL_ADDRESS GetBufferAddress(const size_t MaterialNum) const;

	// Return TextureHandle
	D3D12_GPU_DESCRIPTOR_HANDLE GetTextureHandle(size_t MaterialNum, TEXTURE_USAGE TexUsage) const;

	// Return MaterialCount
	const size_t GetMaterialCount() const;

private:
	Material(const Material&) = delete;
	void operator=(const Material&) = delete;

	// Subset
	struct Subset
	{
		ConstantBuffer* pConstantBuffer = nullptr;
		D3D12_GPU_DESCRIPTOR_HANDLE TextureHandle[TEXTURE_USAGE_COUNT];
	};

	std::map<std::wstring, Texture*> TextureMap = {};
	std::vector<Subset> SubsetList;
	ID3D12Device* pDevice = nullptr;
	DescriptorPool* pPool = nullptr;
};

constexpr auto TU_DIFFUSE = Material::TEXTURE_USAGE_DIFFUSE;
constexpr auto TU_SPECULAR = Material::TEXTURE_USAGE_SPECULAR;
constexpr auto TU_SHININESS = Material::TEXTURE_USAGE_SHININESS;
constexpr auto TU_NORMAL = Material::TEXTURE_USAGE_NORMAL;