#pragma once

#include <ResourceMesh.h>
#include <VertexBuffer.h>
#include <IndexBuffer.h>

// Mesh
class Mesh
{
public:
	// Ctr & Dtr
	Mesh();
	virtual ~Mesh();

	// Initialize
	const bool Initialize(ID3D12Device* pDevice, const ResourceMesh& Resource);

	// Terminate
	void Terminate();

	// Draw
	void Draw(ID3D12GraphicsCommandList* pCmdList);

	// Return MaterialID
	const uint32_t GetMaterialID() const;

private:
	Mesh(const Mesh&) = delete;
	void operator=(const Mesh&) = delete;

	VertexBuffer VertexBuffer_ = {};
	IndexBuffer IndexBuffer_ = {};
	uint32_t MaterialID = 0;
	uint32_t IndexCount = 0;
};
