#pragma once

#include <cstdint>
#include <mutex>
#include <cassert>
#include <functional>

template<typename T>
class Pool
{
public:
	Pool()
		: pBuffer(nullptr)
		, pActiveHead(nullptr)
		, pFreeHead(nullptr)
		, ItemCapacity(0)
		, ItemNum(0)
	{}

	~Pool()
	{}

	// Init
	const bool Initialize(uint32_t Capacity)
	{
		std::lock_guard<std::mutex> Guard(Mutex_);
		pBuffer = static_cast<uint8_t*>(malloc(sizeof(Item) * (Capacity + 2)));
		if(!pBuffer)
		{
			return false;
		}

		ItemCapacity = Capacity;

		// Assign Index
		for(auto i = 2u, j = 0u; i < ItemCapacity + 2; ++i, ++j)
		{
			auto TmpItem = GetItem(i);
			if(TmpItem)
			{
				TmpItem->Index = j;
			}
		}

		pActiveHead = GetItem(0);
		pActiveHead->pPrev = pActiveHead->pNext = pActiveHead;
		pActiveHead->Index = uint32_t(-1);

		pFreeHead = GetItem(1);
		pFreeHead->Index = uint32_t(-2);

		for(auto i = 1u; i < ItemCapacity + 2; ++i)
		{
			GetItem(i)->pPrev = nullptr;
			GetItem(i)->pNext = GetItem(i + 1);
		}

		GetItem(ItemCapacity + 1)->pPrev = pFreeHead;

		ItemNum = 0;

		return true;
	}

	// Finalize
	void Terminate()
	{
		std::lock_guard<std::mutex> Guard(Mutex_);
		if(pBuffer)
		{
			free(pBuffer);
			pBuffer = nullptr;
		}

		pActiveHead = nullptr;
		pFreeHead = nullptr;
		ItemCapacity = 0;
		ItemNum = 0;
	}

	// Reserve item
	T* Alloc(std::function<void(uint32_t, T*)> Func = nullptr)
	{
		std::lock_guard<std::mutex> Guard(Mutex_);

		if(pFreeHead->pNext == pFreeHead || ItemNum + 1 > ItemCapacity)
		{
			return nullptr;
		}

		auto TempItem = pFreeHead->pNext;
		pFreeHead->pNext = TempItem->pNext;

		TempItem->pPrev = pActiveHead->pPrev;
		TempItem->pNext = pActiveHead;
		TempItem->pPrev->pNext = TempItem->pNext->pPrev = TempItem;

		++ItemNum;

		// Assign memory
		auto Val = new((void*)TempItem) T();

		// Call UserDefinedInitFunc 
		if(Func)
		{
			Func(TempItem->Index, Val);
		}

		return Val;
	}

	// Free Item
	void Free(T* pItem)
	{
		// Exclude nullptr.
		if(!pItem)
		{
			OutputDebugStringW(L"Pool.h (Free): pItem is nullptr. \n");
			return;
		}

		std::lock_guard<std::mutex> Guard(Mutex_);

		auto TempItem = reinterpret_cast<Item*>(pItem);
		TempItem->pPrev->pNext = TempItem->pNext;
		TempItem->pNext->pPrev = TempItem->pPrev;

		TempItem->pPrev = nullptr;
		TempItem->pNext = pFreeHead->pNext;

		pFreeHead->pNext = TempItem;

		--ItemNum;
	}

	const uint32_t GetItemCapacity() const {
		return ItemCapacity;
	}

	const uint32_t GetItemNum() const {
		return ItemNum;
	}

	const uint32_t GetAvailableItemNum() const {
		return ItemCapacity - ItemNum;
	}

private:
	struct Item
	{
		T Value_;
		uint32_t Index = 0;
		Item* pNext = nullptr;
		Item* pPrev = nullptr;
	
		Item()
			: Value_(0)
			, Index(0)
			, pNext(nullptr)
			, pPrev(nullptr)
		{}

		~Item()
		{}
	};

	uint8_t* pBuffer = nullptr;
	Item* pActiveHead = nullptr;
	Item* pFreeHead = nullptr;
	uint32_t ItemCapacity = 0;
	uint32_t ItemNum = 0;
	std::mutex Mutex_ = {};

	// Pick Item
	Item* GetItem(uint32_t Index)
	{
		assert(0 <= Index && Index <= ItemCapacity + 2);
		return reinterpret_cast<Item*>(pBuffer + sizeof(Item) * Index);
	}

	Item* AssignMemoryForItem(uint32_t Index)
	{
		assert(0 <= Index && Index <= ItemCapacity + 2);
		auto Buf = (pBuffer + sizeof(Item) * Index);

		return new(Buf) Item;
	}

	Pool (const Pool&) = delete;
	void operator=(const Pool&) = delete;
};