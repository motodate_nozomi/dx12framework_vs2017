#pragma once

#include <d3d12.h>
#include <DirectXMath.h>
#include <string>
#include <vector>

struct ResourceMaterial 
{
	DirectX::XMFLOAT3 Diffuse;
	DirectX::XMFLOAT3 Specular;
	float Alpha;
	float Shininess;
	std::wstring DiffuseMap;
	std::wstring SpecularMap;
	std::wstring ShininessMap;
	std::wstring NormalMap;
};

class MeshVertex
{
public:
    MeshVertex() = default;

    MeshVertex(const DirectX::XMFLOAT3& Position,
        const DirectX::XMFLOAT3& Normal,
        const DirectX::XMFLOAT2& TexCoord,
        const DirectX::XMFLOAT3& Tangent )
        : Position(Position)
        , Normal(Normal)
        , TexCoord(TexCoord)
        , Tangent(Tangent)
    {}

    DirectX::XMFLOAT3 Position = {};
    DirectX::XMFLOAT3 Normal = {};
    DirectX::XMFLOAT2 TexCoord = {};
    DirectX::XMFLOAT3 Tangent = {};
    
    static const D3D12_INPUT_LAYOUT_DESC InputLayout;

private:
    static const int InputElementCount = 4;
    static const D3D12_INPUT_ELEMENT_DESC InputElements[InputElementCount];
};

struct ResourceMesh
{
	std::vector<MeshVertex> Vertices;
	std::vector<uint32_t> Indices;
	uint32_t MaterialID;
};

// Load Mesh
const bool LoadMesh(
	const wchar_t* FileName,
	std::vector<ResourceMesh>& MeshList,
	std::vector<ResourceMaterial>& MaterialList
);