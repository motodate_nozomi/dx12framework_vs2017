#pragma once

#include <d3d12.h>
#include <ComPtr.h>
#include <ResourceUploadBatch.h>

class DescriptorHandle;
class DescriptorPool;

class Texture
{
public:
	Texture();
	~Texture();

	// Initialize
	const bool Initialize(
		ID3D12Device* pDevice,
		DescriptorPool* pPool,
		const wchar_t* FileName,
		DirectX::ResourceUploadBatch& Batch
	);

	const bool Initialize(
		ID3D12Device* pDevice,
		DescriptorPool* pPool,
		const D3D12_RESOURCE_DESC* pDesc,
		const bool bIsCube
	);

	// Terminate
	void Terminate();

	// Return CPUHandle
	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUHandle() const;

	// Return GPUHandle
	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUHandle() const;

private:
	Texture(const Texture&) = delete;
	void operator=(const Texture&) = delete;

	D3D12_SHADER_RESOURCE_VIEW_DESC GetViewDesc(const bool bIsCube);

	ComPtr<ID3D12Resource> pTexture = nullptr;
	DescriptorHandle* pHandle = nullptr;
	DescriptorPool* pPool = nullptr;
};