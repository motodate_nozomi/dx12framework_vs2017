#pragma once

#include <d3d12.h>
#include <ComPtr.h>

class VertexBuffer
{
public:
	// Ctr & Dtr
	VertexBuffer();
	~VertexBuffer();

	// Initialize
	const bool Initialize(ID3D12Device* pDevice, const size_t Size, const size_t Stride, const void* pInitData = nullptr);

	template<typename T>
	const bool Initialize(ID3D12Device* pDevice, const size_t Size, const T* pInitData = nullptr)
	{
		return Initialize(pDevice, Size, sizeof(T), pInitData);
	}

	// Terminate
	void Terminate();

	// Mapping
	void* Map();

	template<typename T>
	T* Map() const 
	{
		return reinterpret_cast<T*>(Map());
	}

	// Unmap
	void Unmap();

	// Return VBView
	D3D12_VERTEX_BUFFER_VIEW GetView() const;

private:
	VertexBuffer(const VertexBuffer&) = delete;
	void operator=(const VertexBuffer&) = delete;

	ComPtr<ID3D12Resource> pVertexBuffer = nullptr;
	D3D12_VERTEX_BUFFER_VIEW VBView = {};
};