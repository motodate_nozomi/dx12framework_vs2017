#include <cassert>

#include "App.h"

// Utility
namespace {
    const auto ClassName = TEXT("Window");

    template<typename T>
    void SafeRelease(T*& Ptr)
    {
        if (Ptr)
        {
            Ptr->Release();
            Ptr = nullptr;
        }
    }

    struct Vertex
    {
        DirectX::XMFLOAT3 Position;
        DirectX::XMFLOAT2 UV;
    };

} // end of namespace

App::App(const uint32_t Width, const uint32_t Height)
    : HInstance(nullptr)
    , HWnd(nullptr)
    , Width(Width)
    , Height(Height)
{}

App::~App()
{}

void App::Run()
{
    if (InitializeApp())
    {
        MainLoop();
    }

    TerminateApp();
}

const bool App::InitializeApp()
{
	// Initialize window
    if (!InitializeWindow())
    {
        return false;
    }

	// Initialize dx12
    if (!InitializeD3D())
    {
        return false;
    }

	// Initialize AppCore
    if (!OnInitialize())
    {
        return false;
    }

	// Show Window
	ShowWindow(HWnd, SW_SHOWNORMAL);

	// Update Window
	UpdateWindow(HWnd);

	// Focus Window
	SetFocus(HWnd);

	// Success
    return true;
}

void App::TerminateApp()
{
    // Delete AppData
    OnTerminate();

    // Delete D3D
    TerminateD3D();

    // Delete Window
    TerminateWindow();
}

const bool App::InitializeWindow()
{
    // get instanceHandle
    auto HInst = GetModuleHandle(nullptr);
    if (!HInst)
    {
        OutputDebugStringW(L"Not find HInstance.\n");
        return false;
    }

    // set windowConf
    WNDCLASSEX WC = {};
    WC.cbSize = sizeof(WNDCLASSEX);
    WC.style = CS_HREDRAW | CS_VREDRAW;
    WC.lpfnWndProc = WndProc;
    WC.hIcon = LoadIcon(HInst, IDI_APPLICATION);
    WC.hCursor = LoadCursor(HInst, IDC_ARROW);
    WC.hbrBackground = GetSysColorBrush(COLOR_BACKGROUND);
    WC.lpszMenuName = nullptr;
    WC.lpszClassName = ClassName;
    WC.hIconSm = LoadIcon(HInst, IDI_APPLICATION);

    // register window
    if (!RegisterClassEx(&WC))
    {
        OutputDebugStringW(L"Failed Register Window.\n");
        return false;
    }

    this->HInstance = HInst;

    // set windowSize
    RECT RC = {};
    RC.right = static_cast<LONG>(Width);
    RC.bottom = static_cast<LONG>(Height);

    // adjust windowSize
    auto Style = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU;
    AdjustWindowRect(&RC, Style, FALSE);

    // create window
    HWnd = CreateWindowEx(
        0,
        ClassName,
        TEXT("Framework"),
        Style,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        RC.right - RC.left,
        RC.bottom - RC.top,
        nullptr,
        nullptr,
        HInstance,
        nullptr);

    if (!HWnd)
    {
        OutputDebugStringW(L"Failed Create Window.\n");
        return false;
    }

    ShowWindow(HWnd, SW_SHOWNORMAL);

    UpdateWindow(HWnd);

    SetFocus(HWnd);

    return true;
}

void App::TerminateWindow()
{
    if (HInstance)
    {
        UnregisterClass(ClassName, HInstance);
    }

    HInstance = nullptr;
    HWnd = nullptr;
}


bool App::InitializeD3D()
{
    // Enable DebugLayer
    #if defined(DEBUG) || defined(_DEBUG)
    {
        ComPtr<ID3D12Debug> Debug = nullptr;
        auto HR = D3D12GetDebugInterface(IID_PPV_ARGS(Debug.GetAddressOf()));
        if (SUCCEEDED(HR))
        {
            Debug->EnableDebugLayer();
        }
    }
    #endif

    // create device
    auto HR = D3D12CreateDevice(
        nullptr,
        D3D_FEATURE_LEVEL_11_0,
        IID_PPV_ARGS(D3D12Device.GetAddressOf())
    );
    
    // create commandQueue
    {
        D3D12_COMMAND_QUEUE_DESC Desc = {};
        Desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
        Desc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
        Desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
        Desc.NodeMask = 0;

        HR = D3D12Device->CreateCommandQueue(&Desc, IID_PPV_ARGS(CommandQueue.GetAddressOf()));
    }

    // create swapChain
    {
        ComPtr<IDXGIFactory4> Factory = nullptr;
        HR = CreateDXGIFactory1(IID_PPV_ARGS(Factory.GetAddressOf()));
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed create dxgiFactory for swapChain.");
            return false;
        }

        DXGI_SWAP_CHAIN_DESC Desc = {};
        Desc.BufferDesc.Width = this->Width;
        Desc.BufferDesc.Height = this->Height;
        Desc.BufferDesc.RefreshRate.Numerator = 60;
        Desc.BufferDesc.RefreshRate.Denominator = 1;
        Desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
        Desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
        Desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        Desc.SampleDesc.Count = 1;
        Desc.SampleDesc.Quality = 0;
        Desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
        Desc.BufferCount = FrameCount;
        Desc.OutputWindow = HWnd;
        Desc.Windowed = TRUE;
        Desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
        Desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

        ComPtr<IDXGISwapChain> TempSwapChain = nullptr;
        HR = Factory->CreateSwapChain(CommandQueue.Get(), &Desc, TempSwapChain.GetAddressOf());
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed create TempswapChain.\n");
            return false;
        }

        HR = TempSwapChain.As(&SwapChain);
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed create swapChain3.\n");
            return false;
        }

        // get buckBufferNum
        FrameIndex = SwapChain->GetCurrentBackBufferIndex();

        Factory.Reset();
        TempSwapChain.Reset();

    }

	// Generate DescrptorPool
	{
		D3D12_DESCRIPTOR_HEAP_DESC Desc = {};
		Desc.NodeMask = 1;
		Desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		Desc.NumDescriptors = 512;
		Desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		// Resource
		if(!DescriptorPool::Create(D3D12Device.Get(), &Desc, &pPool[POOL_TYPE_RES]))
		{
			return false;
		}

		// Sampler
		Desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER;
		Desc.NumDescriptors = 256;
		Desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		if(!DescriptorPool::Create(D3D12Device.Get(), &Desc, &pPool[POOL_TYPE_SMP]))
		{
			return false;
		}

		// RTV
		Desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
		Desc.NumDescriptors = 512;
		Desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		if(DescriptorPool::Create(D3D12Device.Get(), &Desc, &pPool[POOL_TYPE_RTV]))
		{
			return false;
		}
	}

	return true;
}

void App::MainLoop()
{
    MSG Msg = {};

    while (WM_QUIT != Msg.message)
    {
        if (PeekMessage(&Msg, nullptr, 0, 0, PM_REMOVE) == TRUE)
        {
            TranslateMessage(&Msg);
            DispatchMessage(&Msg);
        }
        else
        {
            Render();
        }
    }
}



void App::TerminateD3D()
{
    WaitGPU();

    Fence.Reset();

    HeapRTV.Reset();
    for (auto i = 0u; i < FrameCount; ++i)
    {
        ColorBuffer[i].Reset();
    }

    CommandList.Reset();

    for (auto i = 0u; i < FrameCount; ++i)
    {
        CommandAllocator[i].Reset();
    }

    SwapChain.Reset();

    CommandQueue.Reset();

    D3D12Device.Reset();
}

const bool App::OnInitialize()
{
    // Load Mesh
    {
        std::wstring Path = {};
        if(!SearchFilePath(L"res/teapot/teapot.obj", Path))
        {
            OutputDebugStringW(L"Not find mesh's file. \n");
            return false;
        }

        if (!LoadMesh(Path.c_str(), Meshes, Materials))
        {
            OutputDebugStringW(L"Failed to loading mesh.\n");
            return false;
        }

        // limit meshNum
        assert(Meshes.size() == 1);
    }

    // generate vb
    {
        auto Size = sizeof(MeshVertex) * Meshes[0].Vertices.size();
        auto Vertices = Meshes[0].Vertices.data();

        // heap property
        D3D12_HEAP_PROPERTIES HeapProp = {};
        HeapProp.Type = D3D12_HEAP_TYPE_UPLOAD;
        HeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
        HeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
        HeapProp.CreationNodeMask = 1;
        HeapProp.VisibleNodeMask = 1;

        // set resource
        D3D12_RESOURCE_DESC Desc = {};
        Desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
        Desc.Alignment = 0;
        Desc.Width = Size;
        Desc.Height = 1;
        Desc.DepthOrArraySize = 1;
        Desc.MipLevels = 1;
        Desc.Format = DXGI_FORMAT_UNKNOWN;
        Desc.SampleDesc.Count = 1;
        Desc.SampleDesc.Quality = 0;
        Desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
        Desc.Flags = D3D12_RESOURCE_FLAG_NONE;

        // generate resource
        auto HR = D3D12Device->CreateCommittedResource(
            &HeapProp,
            D3D12_HEAP_FLAG_NONE,
            &Desc,
            D3D12_RESOURCE_STATE_GENERIC_READ,
            nullptr,
            IID_PPV_ARGS(VertexBuffer.GetAddressOf()
            ));

        // mapping
        void* Ptr = nullptr;
        HR = VertexBuffer->Map(0, nullptr, &Ptr);
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to mapping VertexBuffer. \n");
            return false;
        }

        // assign vb 
        memcpy(Ptr, Vertices, Size);
        // unmap
        VertexBuffer->Unmap(0, nullptr);
        
        // create bufferView
        VBView.BufferLocation = VertexBuffer->GetGPUVirtualAddress();
        VBView.SizeInBytes = static_cast<UINT>(Size);
        VBView.StrideInBytes = static_cast<UINT>(sizeof(MeshVertex));
    }

    // Create IndexBuffer
    {
        auto Size = sizeof(uint32_t) * Meshes[0].Indices.size();
        auto Indices = Meshes[0].Indices.data();

        // Define HeapProp
        D3D12_HEAP_PROPERTIES Prop = {};
        Prop.Type = D3D12_HEAP_TYPE_UPLOAD;
        Prop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
        Prop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
        Prop.CreationNodeMask = 1;
        Prop.VisibleNodeMask = 1;

        // Set ResourceDesc
        D3D12_RESOURCE_DESC Desc = {};
        Desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
        Desc.Alignment = 0;
        Desc.Width = Size;
        Desc.Height = 1;
        Desc.DepthOrArraySize = 1;
        Desc.MipLevels = 1;
        Desc.Format = DXGI_FORMAT_UNKNOWN;
        Desc.SampleDesc.Count = 1;
        Desc.SampleDesc.Quality = 0;
        Desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
        Desc.Flags = D3D12_RESOURCE_FLAG_NONE;

        // Generate Resource
        auto HR = D3D12Device->CreateCommittedResource(
            &Prop,
            D3D12_HEAP_FLAG_NONE,
            &Desc,
            D3D12_RESOURCE_STATE_GENERIC_READ,
            nullptr,
            IID_PPV_ARGS(IndexBuffer.GetAddressOf())
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to create IndexBuffer. \n");
            return false;
        }

        // Mapping
        void* Ptr = nullptr;
        HR = IndexBuffer->Map(0, nullptr, &Ptr);
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to Mapping for IndexBuffer. \n");
            return false;
        }

        // Set IndexData to MappingData
        memcpy(Ptr, Indices, Size);

        // Unmap
        IndexBuffer->Unmap(0, nullptr);

        // Set IndexBufferView
        IBView.BufferLocation = IndexBuffer->GetGPUVirtualAddress();
        IBView.Format = DXGI_FORMAT_R32_UINT;
        IBView.SizeInBytes = static_cast<UINT>(Size);
    }

    // generate descHeap for CBV/SRV/UAV
    {
        D3D12_DESCRIPTOR_HEAP_DESC Desc = {};
        Desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
        Desc.NumDescriptors = 3;
        Desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
        Desc.NodeMask = 0;

        auto HR = D3D12Device->CreateDescriptorHeap(
            &Desc,
            IID_PPV_ARGS(HeapCBV_SRV_UAV.GetAddressOf())
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to create DescHeap for ConstantBuffer. \n");
            return false;
        }
    }

    // Generate ConstantBuffer
    {
        D3D12_HEAP_PROPERTIES Props = {};
        Props.Type = D3D12_HEAP_TYPE_UPLOAD;
        Props.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
        Props.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
        Props.CreationNodeMask = 1;
        Props.VisibleNodeMask = 1;

        // Set Resource
        D3D12_RESOURCE_DESC Desc = {};
        Desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
        Desc.Alignment = 0;
        Desc.Width = sizeof(Transform);
        Desc.Height = 1;
        Desc.DepthOrArraySize = 1;
        Desc.MipLevels = 1;
        Desc.Format = DXGI_FORMAT_UNKNOWN;
        Desc.SampleDesc.Count = 1;
        Desc.SampleDesc.Quality = 0;
        Desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
        Desc.Flags = D3D12_RESOURCE_FLAG_NONE;

        auto IncrementSize = D3D12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    
        for (auto i = 0; i < FrameCount; ++i)
        {
            // Create Resource
            auto HR = D3D12Device->CreateCommittedResource(
                &Props,
                D3D12_HEAP_FLAG_NONE,
                &Desc,
                D3D12_RESOURCE_STATE_GENERIC_READ,
                nullptr,
                IID_PPV_ARGS(ConstantBuffer[i].GetAddressOf())
            );

            if (FAILED(HR))
            {
                OutputDebugStringW(L"Failed to create Resource. \n");
                return false;
            }

            auto GPUAddress = ConstantBuffer[i]->GetGPUVirtualAddress();
            auto HandleCPU = HeapCBV_SRV_UAV->GetCPUDescriptorHandleForHeapStart();
            auto HandleGPU = HeapCBV_SRV_UAV->GetGPUDescriptorHandleForHeapStart();

            HandleCPU.ptr += IncrementSize * i;
            HandleGPU.ptr += IncrementSize * i;

            CBView[i].HandleCPU = HandleCPU;
            CBView[i].HandleGPU = HandleGPU;
            CBView[i].Desc.BufferLocation = GPUAddress;
            CBView[i].Desc.SizeInBytes = sizeof(Transform);

            // generate ConstatnBufferView
            D3D12Device->CreateConstantBufferView(&CBView[i].Desc, HandleCPU);

            // Mapping
            HR = ConstantBuffer[i]->Map(0, nullptr, reinterpret_cast<void**>(&CBView[i].Buffer));
            if (FAILED(HR))
            {
                OutputDebugStringW(L"Failed to mapping ConstantBuffer. \n");
                return false;
            }

            auto EyePos = DirectX::XMVectorSet(0.0f, 1.0f, 2.0f, 0.0f);
            auto TargetPos = DirectX::XMVectorZero();
            auto Up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

            auto FovY = DirectX::XMConvertToRadians(37.5f);
            auto Aspect = static_cast<float>(Width) / static_cast<float>(Height);

            // Set TransformMatrix
            CBView[i].Buffer->World = DirectX::XMMatrixIdentity();
            CBView[i].Buffer->View = DirectX::XMMatrixLookAtRH(EyePos, TargetPos, Up);
            CBView[i].Buffer->Proj = DirectX::XMMatrixPerspectiveFovRH(FovY, Aspect, 1.0f, 1000.0f);
        }
    }

    // Create RootSignature
    {
        auto Flag = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;
        Flag |= D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS;
        Flag |= D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;
        Flag |= D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS;

        // Set RootPrm(exist for 32bitData = Param.Constant)
        D3D12_ROOT_PARAMETER Param[2] = {};
        Param[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
        Param[0].Descriptor.ShaderRegister = 0;
        Param[0].Descriptor.RegisterSpace = 0;
        Param[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_VERTEX;
        
        D3D12_DESCRIPTOR_RANGE Range = {};
        Range.RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
        Range.NumDescriptors = 1;
        Range.BaseShaderRegister = 0;
        Range.RegisterSpace = 0;
        Range.OffsetInDescriptorsFromTableStart = 0;

        Param[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
        Param[1].DescriptorTable.NumDescriptorRanges = 1;
        Param[1].DescriptorTable.pDescriptorRanges = &Range;
        Param[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

        // Set StaticSampler
        D3D12_STATIC_SAMPLER_DESC Sampler = {};
        Sampler.Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
        Sampler.AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
        Sampler.AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
        Sampler.AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
        Sampler.MipLODBias = D3D12_DEFAULT_MIP_LOD_BIAS;
        Sampler.MaxAnisotropy = 1;
        Sampler.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
        Sampler.BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
        Sampler.MinLOD = -D3D12_FLOAT32_MAX;
        Sampler.MaxLOD = +D3D12_FLOAT32_MAX;
        Sampler.ShaderRegister = 0;
        Sampler.RegisterSpace = 0;
        Sampler.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

        // Set RootSinature
        D3D12_ROOT_SIGNATURE_DESC Desc = {};
        Desc.NumParameters = 2;
        Desc.NumStaticSamplers = 1;
        Desc.pParameters = Param;
        Desc.pStaticSamplers = &Sampler;
        Desc.Flags = Flag;

        ComPtr<ID3DBlob> Blob = nullptr;
        ComPtr<ID3DBlob> ErrorBlob = nullptr;

        // Serialize
        auto HR = D3D12SerializeRootSignature(
            &Desc,
            D3D_ROOT_SIGNATURE_VERSION_1_0,
            Blob.GetAddressOf(),
            ErrorBlob.GetAddressOf()
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to Serialize.\n");
            return false;
        }

        // Generate RootSignature
        HR = D3D12Device->CreateRootSignature(
            0,
            Blob->GetBufferPointer(),
            Blob->GetBufferSize(),
            IID_PPV_ARGS(RootSignature.GetAddressOf())
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to Crate RootSignature.\n");
            return false;
        }
    }

    // Create PSO
    {
        // Assign InputLayout

        // Set RasterState
        D3D12_RASTERIZER_DESC RSDesc = {};
        RSDesc.FillMode = D3D12_FILL_MODE_SOLID;
        RSDesc.CullMode = D3D12_CULL_MODE_NONE;
        RSDesc.FrontCounterClockwise = FALSE;
        RSDesc.DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
        RSDesc.DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
		RSDesc.SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
        RSDesc.DepthClipEnable = FALSE;
        RSDesc.MultisampleEnable = FALSE;
        RSDesc.AntialiasedLineEnable = FALSE;
        RSDesc.ForcedSampleCount = 0;
        RSDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

        // Generate BlendMode For RenderTarget
        D3D12_RENDER_TARGET_BLEND_DESC RTBlendStateDesc = {
            FALSE, FALSE,
            D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
            D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
            D3D12_LOGIC_OP_NOOP,
            D3D12_COLOR_WRITE_ENABLE_ALL
        };

        // Set BlendState
        D3D12_BLEND_DESC BlendStateDesc = {};
        BlendStateDesc.AlphaToCoverageEnable = FALSE;
        BlendStateDesc.IndependentBlendEnable = FALSE;
        for (UINT i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; ++i)
        {
            BlendStateDesc.RenderTarget[i] = RTBlendStateDesc;
        }

        // Set DepthStencilState
        D3D12_DEPTH_STENCIL_DESC DSSDesc = {};
        DSSDesc.DepthEnable = TRUE;
        DSSDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
        DSSDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
        DSSDesc.StencilEnable = FALSE;

        // Load Shader
        ComPtr<ID3DBlob> VSBlob = nullptr;
        ComPtr<ID3DBlob> PSBlob = nullptr;

        std::wstring VSPath = {};
        std::wstring PSPath = {};

        if (!SearchFilePath(L"SimpleVS.cso", VSPath))
        {
            return false;
        }

        if (!SearchFilePath(L"SimplePS.cso", PSPath))
        {
            return false;
        }

        // VS
        auto HR = D3DReadFileToBlob(VSPath.c_str(), VSBlob.GetAddressOf());
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to Load VS.\n");
            return false;
        }

        // PS
        HR = D3DReadFileToBlob(PSPath.c_str(), PSBlob.GetAddressOf());
        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to Load PS.\n");
            return false;
        }

        // Assign PiplineState
        D3D12_GRAPHICS_PIPELINE_STATE_DESC PipelineDesc = {};
        PipelineDesc.InputLayout = MeshVertex::InputLayout;
        PipelineDesc.pRootSignature = RootSignature.Get();
        PipelineDesc.VS = {VSBlob->GetBufferPointer(), VSBlob->GetBufferSize()};
        PipelineDesc.PS = {PSBlob->GetBufferPointer(), PSBlob->GetBufferSize()};
        PipelineDesc.RasterizerState = RSDesc;
        PipelineDesc.BlendState = BlendStateDesc;
        PipelineDesc.DepthStencilState = DSSDesc;
        PipelineDesc.SampleMask = UINT_MAX;
        PipelineDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
        PipelineDesc.NumRenderTargets = 1;
        PipelineDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
        PipelineDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
        PipelineDesc.SampleDesc.Count = 1;
        PipelineDesc.SampleDesc.Quality = 0;

        // Generate PSO
        HR = D3D12Device->CreateGraphicsPipelineState(
            &PipelineDesc,
            IID_PPV_ARGS(PSO.GetAddressOf())
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to generate PSO.\n");
            return false;
        }
    }

    // Generate Texture
    {
        // Search Texture
        std::wstring TexturePath = {};
        if (!SearchFilePath(L"res/SampleTexture.dds", TexturePath))
        {
            OutputDebugStringW(L"Not find texture.\n");
            return false;
        }

        DirectX::ResourceUploadBatch Bach(D3D12Device.Get());
        Bach.Begin();

        // Create Resource
        auto HR = DirectX::CreateDDSTextureFromFile(
            D3D12Device.Get(),
            Bach,
            TexturePath.c_str(),
            Texture_.pResource.GetAddressOf(),
            true
        );

        if (FAILED(HR))
        {
            OutputDebugStringW(L"Failed to generate texture.\n");
            return false;
        }

        auto Future = Bach.End(CommandQueue.Get());
        Future.wait();

        auto IncrementSize = D3D12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
        auto HandleCPU = HeapCBV_SRV_UAV->GetCPUDescriptorHandleForHeapStart();
        auto HandleGPU = HeapCBV_SRV_UAV->GetGPUDescriptorHandleForHeapStart();

        // perhaps... affected previous increment ?
        HandleCPU.ptr += IncrementSize * 2;
        HandleGPU.ptr += IncrementSize * 2;

        Texture_.HandleCPU = HandleCPU;
        Texture_.HandleGPU = HandleGPU;

        // Get TextureConf
        auto TextureDesc = Texture_.pResource->GetDesc();
        
        // Assign SRV of Conf
        D3D12_SHADER_RESOURCE_VIEW_DESC ViewDesc = {};
        ViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
        ViewDesc.Format = TextureDesc.Format;
        ViewDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
        ViewDesc.Texture2D.MostDetailedMip = 0;
        ViewDesc.Texture2D.MipLevels = TextureDesc.MipLevels;
        ViewDesc.Texture2D.PlaneSlice = 0;
        ViewDesc.Texture2D.ResourceMinLODClamp = 0.0f;

        // Generate SRV
        D3D12Device->CreateShaderResourceView(Texture_.pResource.Get(), &ViewDesc, HandleCPU);
    }

    // Create ViewPort
    {
        Viewport.TopLeftX = 0;
        Viewport.TopLeftY = 0;
        Viewport.Width = static_cast<float>(Width);
        Viewport.Height = static_cast<float>(Height);
        Viewport.MinDepth = 0.0f;
        Viewport.MaxDepth = 1.0f;

        Scissor.left = 0;
        Scissor.right = Width;
        Scissor.top = 0;
        Scissor.bottom = Height;
    }

    CommandList->Close();

    return true;
}

void App::OnTerminate()
{
    // Remove CB
    for (auto i = 0; i < FrameCount; ++i)
    {
        if (ConstantBuffer[i].Get())
        {
            ConstantBuffer[i]->Unmap(0, nullptr);
            memset(&CBView[i], 0, sizeof(ConstantBuffer[i]));
        }
        ConstantBuffer[i].Reset();
    }

    IndexBuffer.Reset();
    VertexBuffer.Reset();
    PSO.Reset();
    HeapCBV_SRV_UAV.Reset();

    VBView.BufferLocation = 0;
    VBView.SizeInBytes = 0;
    VBView.StrideInBytes = 0;

    IBView.BufferLocation = 0;
    IBView.Format = DXGI_FORMAT_UNKNOWN;
    IBView.SizeInBytes = 0;

    RootSignature.Reset();

    Texture_.pResource.Reset();
    Texture_.HandleCPU.ptr = 0;
    Texture_.HandleCPU.ptr = 0;
}

void App::Render()
{
    // Update
    {
        RotateAngle += 0.025f;
        CBView[FrameIndex].Buffer->World =
            DirectX::XMMatrixRotationY(RotateAngle);

		//CBView[FrameIndex].Buffer->World = DirectX::XMMatrixIdentity();
    }

    // record commands
    CommandAllocator[FrameIndex]->Reset();
    CommandList->Reset(CommandAllocator[FrameIndex].Get(), nullptr);

    // set resourceBarrier
    D3D12_RESOURCE_BARRIER Barrier = {};
    Barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    Barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
    Barrier.Transition.pResource = ColorBuffer[FrameIndex].Get();
    Barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
    Barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
    Barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
    CommandList->ResourceBarrier(1, &Barrier);
    
    // set renderTarget
    // handle = GPUのポインタを指す
    CommandList->OMSetRenderTargets(1, &HandleRTV[FrameIndex], FALSE, &HandleDSV);

    // set ClearColor
    constexpr float ClearColor[] = { 0.15f, 0.25f, 0.55f, 1.0f };

    // clear RTV
    CommandList->ClearRenderTargetView(HandleRTV[FrameIndex], ClearColor, 0, nullptr);

    // ClearDSV
    CommandList->ClearDepthStencilView(HandleDSV, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);

    // DrawProcess 
    {
        CommandList->SetGraphicsRootSignature(RootSignature.Get());
        CommandList->SetDescriptorHeaps(1, HeapCBV_SRV_UAV.GetAddressOf());
        CommandList->SetGraphicsRootConstantBufferView(0, CBView[FrameIndex].Desc.BufferLocation);
        CommandList->SetGraphicsRootDescriptorTable(1, Texture_.HandleGPU);
        CommandList->SetPipelineState(PSO.Get());

        CommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
        CommandList->IASetVertexBuffers(0, 1, &VBView);
        CommandList->IASetIndexBuffer(&IBView);
        CommandList->RSSetViewports(1, &Viewport);
        CommandList->RSSetScissorRects(1, &Scissor);

        auto Count = static_cast<uint32_t>(Meshes[0].Indices.size());
        CommandList->DrawIndexedInstanced(Count, 1, 0, 0, 0);

		std::wstring StrCount = L"IndexSize: " + std::to_wstring(Count) + L"\n";
		OutputDebugStringW(StrCount.c_str());
    }

    // set resourceBarrier
    Barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    Barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
    Barrier.Transition.pResource = ColorBuffer[FrameIndex].Get();
    Barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
    Barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
    Barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
    CommandList->ResourceBarrier(1, &Barrier);

    // end record command
    CommandList->Close();

    // execute command
    ID3D12CommandList* ppCmdList[] = { CommandList.Get() };
    CommandQueue->ExecuteCommandLists(1, ppCmdList);

    // show renderResult to display
    Present(1);
}

void App::WaitGPU()
{
    if (!CommandQueue || !Fence || !FenceEvent)
    {
        return;
    }

    CommandQueue->Signal(Fence.Get(), FenceCounter[FrameIndex]);
    Fence->SetEventOnCompletion(FenceCounter[FrameIndex], FenceEvent);

    WaitForSingleObjectEx(FenceEvent, INFINITE, FALSE);

    FenceCounter[FrameIndex]++;
}

void App::Present(uint32_t Interval)
{
    SwapChain->Present(Interval, 0);
    const auto CurrentValue = FenceCounter[FrameIndex];
    CommandQueue->Signal(Fence.Get(), CurrentValue);

    FrameIndex = SwapChain->GetCurrentBackBufferIndex();

    if (Fence->GetCompletedValue() < FenceCounter[FrameIndex])
    {
        Fence->SetEventOnCompletion(FenceCounter[FrameIndex], FenceEvent);
        WaitForSingleObjectEx(FenceEvent, INFINITE, FALSE);
    }

    FenceCounter[FrameIndex] = CurrentValue + 1;
}

LRESULT CALLBACK App::WndProc(HWND HWnd, UINT Msg, WPARAM Wp, LPARAM Lp)
{
    switch (Msg)
    {
        case WM_DESTROY:
        {
            PostQuitMessage(0);
            break;
        }

        default:
        {
            break;
        }
    }

    return DefWindowProc(HWnd, Msg, Wp, Lp);
}