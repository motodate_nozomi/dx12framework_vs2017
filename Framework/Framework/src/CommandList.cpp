#include "CommandList.h"
#include "Logger.h"

CommandList::CommandList()
	: pCmdList(nullptr)
	, pAllocators()
	, AllocatorNumber(0){
}

CommandList::~CommandList()
{
	Terminate();
}

const bool CommandList::Initialize(ID3D12Device* pDevice, D3D12_COMMAND_LIST_TYPE Type, uint32_t AllocatorCount)
{
	// Validate argument.
	if(!pDevice || AllocatorCount <= 0)
	{
		ELOG("Error: Invalid argument. \n");
		return false;
	}
	pAllocators.resize(AllocatorCount);

	// Generate CmdAllocator
	for(auto i = 0u; i < AllocatorCount; ++i)
	{
		auto HR = pDevice->CreateCommandAllocator(
			Type, IID_PPV_ARGS(pAllocators[i].GetAddressOf())
		);

		if(FAILED(HR))
		{
			ELOG("Error: Failed to create CmdAllocator. \n");
			return false;
		}
	}

	// Generate CmdList
	{
		auto HR = pDevice->CreateCommandList(
			1, 
			Type,
			pAllocators[0].Get(),
			nullptr,
			IID_PPV_ARGS(pCmdList.GetAddressOf())
		);

		if(FAILED(HR))
		{
			return false;
		}

		pCmdList->Close();
	}

	AllocatorNumber = 0;

	return true;
}

void CommandList::Terminate()
{
	pCmdList.Reset();

	for(size_t i = 0; i < pAllocators.size(); ++i)
	{
		pAllocators[i].Reset();
	}

	pAllocators.clear();
	pAllocators.shrink_to_fit();
}

ID3D12GraphicsCommandList* CommandList::Reset()
{
	auto HR = pAllocators[AllocatorNumber]->Reset();
	if(FAILED(HR))
	{
		return nullptr;	
	}

	HR = pCmdList->Reset(pAllocators[AllocatorNumber].Get(), nullptr);
	if(FAILED(HR))
	{
		return nullptr;
	}

	AllocatorNumber = (AllocatorNumber + 1) % uint32_t(pAllocators.size());

	return pCmdList.Get();
}
