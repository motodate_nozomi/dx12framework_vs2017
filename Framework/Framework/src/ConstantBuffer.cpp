#include "ConstantBuffer.h"
#include "DescriptorPool.h"


ConstantBuffer::ConstantBuffer()
: pConstantBuffer(nullptr)
, pHandle(nullptr)
, pPool(nullptr)
, MappedPtr(nullptr) {
}

ConstantBuffer::~ConstantBuffer()
{
	Terminate();
}

const bool ConstantBuffer::Initialize(ID3D12Device* pDevice, DescriptorPool* pPool, size_t Size)
{
	if(!pDevice || !pPool || Size <= 0)
	{
		OutputDebugStringW(L"Contains nullArg. \n");
		return false;
	}

	assert(!this->pPool);
	assert(!this->pHandle);

	this->pPool = pPool;
	this->pPool->AddReff();

	constexpr size_t Align = D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT;
	UINT64 SizeAligned = (Size + (Align - 1)) & ~(Align - 1); // round up to Align

	// HeapProp
	D3D12_HEAP_PROPERTIES Prop = {};
	Prop.Type = D3D12_HEAP_TYPE_UPLOAD;
	Prop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;

	// success
	return true;
}

void ConstantBuffer::Terminate()
{
	if(pConstantBuffer)
	{
		pConstantBuffer->Unmap(0, nullptr);
		pConstantBuffer.Reset();
	}

	if(pPool)
	{
		pPool->FreeHandle(pHandle);
		pPool->Release();
		pPool = nullptr;
	}

	MappedPtr = nullptr;
}

D3D12_GPU_VIRTUAL_ADDRESS ConstantBuffer::GetAddress() const
{
	return CBDesc.BufferLocation;
}

D3D12_CPU_DESCRIPTOR_HANDLE ConstantBuffer::GetCPUHandle() const
{
	if(!pHandle)
	{
		return D3D12_CPU_DESCRIPTOR_HANDLE();
	}

	return pHandle->CPUHandle;
}

D3D12_GPU_DESCRIPTOR_HANDLE ConstantBuffer::GetGPUHandle() const
{
	if(!pHandle)
	{
		return D3D12_GPU_DESCRIPTOR_HANDLE();
	}

	return pHandle->GPUHandle;
}

void* ConstantBuffer::GetPtr() const
{
	return MappedPtr;
}
