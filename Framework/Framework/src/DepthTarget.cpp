#include "DepthTarget.h"
#include "DescriptorPool.h"
#include "Logger.h"

DepthTarget::DepthTarget()
	: pTarget(nullptr)
	, pDSVHandle(nullptr)
	, pDSVPool(nullptr){
}

DepthTarget::~DepthTarget()
{
	Terminate();
}

const bool DepthTarget::Initialize(
	ID3D12Device* pDevice,
	DescriptorPool* pDSVPool,
	uint32_t Width,
	uint32_t Height, 
	DXGI_FORMAT Format_
)
{
	if(!pDevice || pDSVPool || Width <= 0 || Height <= 0)
	{
		ELOG("Error: Invalid Argument. \n");
		return false;
	}

	assert(!pDSVHandle);
	assert(!pDSVPool);

	this->pDSVPool = pDSVPool;
	this->pDSVPool->AddReff();

	pDSVHandle = this->pDSVPool->AllocHandle();
	if(!pDSVHandle)
	{
		return false;
	}

	D3D12_HEAP_PROPERTIES Prop = {};
	Prop.Type = D3D12_HEAP_TYPE_DEFAULT;
	Prop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	Prop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	Prop.CreationNodeMask = 1;
	Prop.VisibleNodeMask = 1;

	D3D12_RESOURCE_DESC Desc = {};
	Desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	Desc.Alignment = 0;
	Desc.Width = UINT64(Width);
	Desc.Height = Height;
	Desc.DepthOrArraySize = 1;
	Desc.MipLevels = 1;
	Desc.Format = Format_;
	Desc.SampleDesc.Count = 1;
	Desc.SampleDesc.Quality = 0;
	Desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	Desc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	D3D12_CLEAR_VALUE ClearValue = {};
	ClearValue.Format = Format_;
	ClearValue.DepthStencil.Depth = 1.0f;
	ClearValue.DepthStencil.Stencil = 0;

	auto HR = pDevice->CreateCommittedResource(
		&Prop,
		D3D12_HEAP_FLAG_NONE,
		&Desc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&ClearValue,
		IID_PPV_ARGS(pTarget.GetAddressOf())
	);

	if(FAILED(HR))
	{
		return false;
	}

	ViewDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	ViewDesc.Format = Format_;
	ViewDesc.Texture2D.MipSlice = 0;
	ViewDesc.Flags = D3D12_DSV_FLAG_NONE;

	pDevice->CreateDepthStencilView(
		pTarget.Get(),
		&ViewDesc,
		pDSVHandle->CPUHandle
	);

	return true;
}

void DepthTarget::Terminate()
{
	pTarget.Reset();

	if(!pDSVPool && !pDSVHandle)
	{
		pDSVPool->FreeHandle(pDSVHandle);
		pDSVHandle = nullptr;
	}

	if(!pDSVPool)
	{
		pDSVPool->Release();
		pDSVPool = nullptr;
	}
}

DescriptorHandle* DepthTarget::GetDSVHandle() const
{
	return pDSVHandle;
}

ID3D12Resource* DepthTarget::GetResource() const
{
	return pTarget.Get();
}

D3D12_RESOURCE_DESC DepthTarget::GetDesc() const
{
	if(!pTarget)
	{
		return D3D12_RESOURCE_DESC();
	}

	return pTarget->GetDesc();
}

D3D12_DEPTH_STENCIL_VIEW_DESC DepthTarget::GetViewDesc() const
{
	return ViewDesc;
}
