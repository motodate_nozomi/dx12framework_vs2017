#include "DescriptorPool.h"

const bool DescriptorPool::Create(ID3D12Device* pDevice, const D3D12_DESCRIPTOR_HEAP_DESC* pDesc, DescriptorPool** ppPool)
{
	if(!pDevice || !pDesc || !ppPool)
	{
		OutputDebugStringW(L"DescriptorPool(Create): Contains nullArg. \n");
		return false;
	}

	// Generate Instance
	auto Instance = new(std::nothrow) DescriptorPool();
	if(!Instance)
	{
		OutputDebugStringW(L"DescriptorPool(Create): Contains nullArg. \n");
		return false;
	}

	// Generate DesHeap
	auto HR = pDevice->CreateDescriptorHeap(
		pDesc,
		IID_PPV_ARGS(Instance->pHeap.GetAddressOf())
	);

	if(FAILED(HR))
	{
		Instance->Release();
		OutputDebugStringW(L"DescriptorPool(Create): Failed to generating instance. \n");
		return false;
	}

	// Init handle
	if(!Instance->HandlePool.Initialize(pDesc->NumDescriptors))
	{
		Instance->Release();
		OutputDebugStringW(L"DescriptorPool(Create): Failed to initialize HandlePool. \n");
		return false;
	}

	// Calculate DesSize
	Instance->DescriptorSize = pDevice->GetDescriptorHandleIncrementSize(pDesc->Type);

	// Set Instance
	*ppPool = Instance;

	// success
	return true;
}

void DescriptorPool::AddReff()
{
	++RefCount;
}

void DescriptorPool::Release()
{
	--RefCount;
	if(RefCount <= 0)
	{
		delete this;
	}
}

const uint32_t DescriptorPool::GetRefCount() const
{
	return RefCount;
}

DescriptorHandle* DescriptorPool::AllocHandle()
{
	// InitializeFunc
	auto InitFunc = [&](const uint32_t Index, DescriptorHandle* pHandle)
	{
		auto CPUHandle = pHeap->GetCPUDescriptorHandleForHeapStart();
		CPUHandle.ptr += DescriptorSize * Index;

		auto GPUHandle = pHeap->GetGPUDescriptorHandleForHeapStart();
		GPUHandle.ptr += DescriptorSize * Index;

		pHandle->CPUHandle = CPUHandle;
		pHandle->GPUHandle = GPUHandle;
	};

	return HandlePool.Alloc(InitFunc);
}

void DescriptorPool::FreeHandle(DescriptorHandle*& pHandle)
{
	if(!pHandle)
	{
		HandlePool.Free(pHandle);

		pHandle = nullptr;
	}
}

const uint32_t DescriptorPool::GetAvailableHandleCount() const
{
	return HandlePool.GetAvailableItemNum();
}

// Return UsedHandle
const uint32_t DescriptorPool::GetHandleNum() const
{
	return HandlePool.GetItemNum();
}

const uint32_t DescriptorPool::GetHandleCapacity() const
{
	return HandlePool.GetItemCapacity();
}

ID3D12DescriptorHeap* const DescriptorPool::GetHeap() const
{
	return pHeap.Get();
}

// Ctr
DescriptorPool::DescriptorPool()
: RefCount(1)
, HandlePool()
, pHeap(nullptr)
, DescriptorSize(0){
}

// Dtr
DescriptorPool::~DescriptorPool()
{
	HandlePool.Terminate();
	
	if(pHeap)
	{
		pHeap.Reset();
	}

	DescriptorSize = 0;
}