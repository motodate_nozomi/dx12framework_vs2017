#include "Fence.h"
#include "Logger.h"

Fence::Fence()
	: pFence(nullptr)
	, Event_()
	, Counter(0){
}

Fence::~Fence()
{
	Terminate();
}

const bool Fence::Initialize(ID3D12Device* pDevice)
{
	if(!pDevice)
	{
		ELOG("Error: Failed to Initialize. \n");
		return false;
	}

	// Generate Event
	Event_ = CreateEventEx(nullptr, FALSE, FALSE, EVENT_ALL_ACCESS);
	if(!Event_)
	{
		return false;
	}

	// Generate Fence
	auto HR = pDevice->CreateFence(
		0,
		D3D12_FENCE_FLAG_NONE,
		IID_PPV_ARGS(pFence.GetAddressOf())
	);

	// Set Counter
	Counter = 1;

	return true;
}

void Fence::Terminate()
{
	// Close handle
	if(!Event_)
	{
		CloseHandle(Event_);
		Event_ = nullptr;
	}

	// Remove FenceObject
	pFence.Reset();

	// Reset Counter
	Counter = 0;
}

void Fence::Wait(ID3D12CommandQueue* pQueue, UINT Timeout)
{
	if(!pQueue)
	{
		ELOG("Error: Not find CommandQueue. \n");
		return;
	}

	const auto FenceValue = Counter;

	// SignalProcess
	auto HR = pQueue->Signal(pFence.Get(), FenceValue);
	if(FAILED(HR))
	{
		return;
	}

	// Increase counter
	++Counter;

	// Wait if NextFrameState is not ready
	if(pFence->GetCompletedValue() < FenceValue)
	{
		// Set Event When completed
		auto HR = pFence->SetEventOnCompletion(FenceValue, Event_);
		if(FAILED(HR))
		{
			return;
		}

		// WaitProcess
		if(WAIT_OBJECT_0 != WaitForSingleObjectEx(Event_, Timeout, FALSE))
		{
			return;
		}
	}
}

void Fence::Sync(ID3D12CommandQueue* pQueue)
{
	if(!pQueue)
	{	
		ELOG("Error: Not find CommandQueue. \n");
		return;
	}

	// SignalProcess
	auto HR = pQueue->Signal(pFence.Get(), Counter);
	if(FAILED(HR))
	{
		return;
	}

	// Set event when completed
	HR = pFence->SetEventOnCompletion(Counter, Event_);
	if(FAILED(HR))
	{
		return;
	}

	// WiatProcess
	if(WAIT_OBJECT_0 != WaitForSingleObjectEx(Event_, INFINITE, FALSE))
	{
		return;	
	}

	// Increase Counter
	++Counter;
}
