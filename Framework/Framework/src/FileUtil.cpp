#include "FileUtil.h"

const bool SearchFilePahtA(const char* FileName, std::string& Result)
{
    // ファイル名がない場合は見つけられない
    if (!FileName)
    {
        return false;
    }

    if (!strcmp(FileName, " ") || !strcmp(FileName, ""))
    {
        return false;
    }

    // 親フォルダ取得
    char ExePath[520] = {};
    GetModuleFileNameA(nullptr, ExePath, 520);
    ExePath[519] = L'\0';
    PathRemoveFileSpecA(ExePath);

    char DistPath[520] = {};
    strcpy_s(DistPath, FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }

    sprintf_s(DistPath, "..\\%s", FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }

    sprintf_s(DistPath, "..\\..\\%s", FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }

    sprintf_s(DistPath, "\\res\\%s", FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }

    // %EXE_DIR%
    sprintf_s(DistPath, "%s\\%s", ExePath, FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }


    sprintf_s(DistPath, "%s\\..\\%s", ExePath, FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }

    sprintf_s(DistPath, "%s\\..\\..\\%s", ExePath, FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }

    sprintf_s(DistPath, "%s\\res\\%s", ExePath, FileName);
    if (PathFileExistsA(DistPath))
    {
        Result = DistPath;
        return true;
    }

    return false;
}

const bool SearchFilePathW(const wchar_t* FileName, std::wstring& Result)
{
    // ファイル名がない場合は見つけられない
    if (!FileName)
    {
        return false;
    }

    if (!wcscmp(FileName, L" ") || !wcscmp(FileName, L""))
    {
        return false;
    }

    // 親フォルダ取得
    wchar_t ExePath[520] = {};
    GetModuleFileNameW(nullptr, ExePath, 520);
    ExePath[519] = L'\0';
    PathRemoveFileSpecW(ExePath);

    wchar_t DistPath[520] = {};
    wcscpy_s(DistPath, FileName);
    if(PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }

    swprintf_s(DistPath, L"..\\%s", FileName);
    if(PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }

    swprintf_s(DistPath, L"..\\..\\%s", FileName);
    if (PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }

    swprintf_s(DistPath, L"\\res\\%s", FileName);
    if (PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }

    // %EXE_DIR%
    swprintf_s(DistPath, L"%s\\%s", ExePath, FileName);
    if (PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }


    swprintf_s(DistPath, L"%s\\..\\%s", ExePath, FileName);
    if (PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }

    swprintf_s(DistPath, L"%s\\..\\..\\%s", ExePath, FileName);
    if (PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }

    swprintf_s(DistPath, L"%s\\res\\%s", ExePath, FileName);
    if (PathFileExistsW(DistPath))
    {
        Result = DistPath;
        return true;
    }

    return false;
}