#include "IndexBuffer.h"

IndexBuffer::IndexBuffer()
: pIndexBuffer(nullptr) {
	memset(&IBView, 0, sizeof(IBView));
}

IndexBuffer::~IndexBuffer()
{
	Terminate();
}

const bool IndexBuffer::Initialize(ID3D12Device* pDevice, const size_t Size, const uint32_t* pInitData /*= nullptr*/)
{
	// HeapProp
	D3D12_HEAP_PROPERTIES Prop = {};
	Prop.Type = D3D12_HEAP_TYPE_UPLOAD;
	Prop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	Prop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	Prop.CreationNodeMask = 1;
	Prop.VisibleNodeMask = 1;

	// Set Resource
	D3D12_RESOURCE_DESC Desc = {};
	Desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	Desc.Alignment = 0;
	Desc.Width = UINT64(Size);
	Desc.Height = 1;
	Desc.DepthOrArraySize = 1;
	Desc.MipLevels = 1;
	Desc.Format = DXGI_FORMAT_UNKNOWN;
	Desc.SampleDesc.Count = 1;
	Desc.SampleDesc.Quality = 0;
	Desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	Desc.Flags = D3D12_RESOURCE_FLAG_NONE;

	// Generate Resource
	auto HR = pDevice->CreateCommittedResource(
		&Prop,
		D3D12_HEAP_FLAG_NONE,
		&Desc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(pIndexBuffer.GetAddressOf())
	);

	if(FAILED(HR))
	{
		OutputDebugStringW(L"IndexBuffer(Initialize): Failed generate resource. \n");
		return false;
	}

	// Set IndexBufferView
	IBView.BufferLocation = pIndexBuffer->GetGPUVirtualAddress();
	IBView.Format = DXGI_FORMAT_R32_UINT;
	IBView.SizeInBytes = UINT(Size);

	// if initData that write
	if(!pInitData)
	{
		void* Ptr = Map();
		if(!Ptr)
		{
			OutputDebugStringW(L"IndexBuffer(Initialize): Failed to mapping. \n");
			return false;
		}
	}

	// success
	return true;
}

void IndexBuffer::Terminate()
{
	pIndexBuffer.Reset();
	memset(&IBView, 0, sizeof(IBView));
}

uint32_t* IndexBuffer::Map()
{
	uint32_t* Ptr = nullptr;
	auto HR = pIndexBuffer->Map(0, nullptr, reinterpret_cast<void**>(&Ptr));

	if(FAILED(HR))
	{
		return nullptr;
	}

	return Ptr;
}

void IndexBuffer::Unmap()
{
	pIndexBuffer->Unmap(0, nullptr);
}

D3D12_INDEX_BUFFER_VIEW IndexBuffer::GetView() const
{
	return IBView;
}
