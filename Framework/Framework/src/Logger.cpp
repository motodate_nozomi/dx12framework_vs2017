#include "Logger.h"

#include <cstdio>
#include <cstdarg>
#include <Windows.h>

void OutputLog(const char* Format_...)
{
	char Msg[2048];

	memset(Msg, '\0', sizeof(Msg));
	va_list Arg;

	va_start(Arg, Format_);
	vsprintf_s(Msg, Format_, Arg);
	va_end(Arg);

	// Output to console
	printf_s("%s", Msg);

	OutputDebugStringA(Msg);
}