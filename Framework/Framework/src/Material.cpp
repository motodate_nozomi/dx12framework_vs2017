#include "Material.h"
#include "FileUtil.h"
#include "Logger.h"

namespace {
	const wchar_t* DummyTag = L"";
} // end of namespace

// MaterialClass
Material::Material()
	: pDevice(nullptr)
	, pPool(nullptr)
{}

Material::~Material()
{
	Terminate();
}

// Initialize
const bool Material::Initialize(ID3D12Device* pDevice, DescriptorPool* pPool, size_t BufferSize, size_t MaterialCount)
{
	if(!pDevice || !pPool || MaterialCount <= 0)
	{
		ELOG("Error: Invalid argument. \n");
		return false;
	}

	Terminate();

	this->pDevice = pDevice;
	this->pDevice->AddRef();

	this->pPool = pPool;
	pPool->AddReff();

	SubsetList.resize(MaterialCount);

	// Generate DummyTex
	{
		auto pTexture = new(std::nothrow) Texture();
		if(!pTexture)
		{
			ELOG("Error: Failed to generate DummyTex. \n");
			return false;
		}

		D3D12_RESOURCE_DESC Desc = {};
		Desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
		Desc.Width = 1;
		Desc.Height = 1;
		Desc.DepthOrArraySize = 1;
		Desc.MipLevels = 1;
		Desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		Desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
		Desc.SampleDesc.Count = 1;
		Desc.SampleDesc.Quality = 0;

		if(!pTexture->Initialize(this->pDevice, this->pPool, &Desc, false))
		{
			ELOG("Error: Failed to Initialize texture. \n");
			pTexture->Terminate();
			delete pTexture;
			return false;
		}

		TextureMap[DummyTag] = pTexture;
	}

	auto Size_ = BufferSize * MaterialCount;
	if(Size_ > 0)
	{
		for(size_t i = 0; i < SubsetList.size(); ++i)
		{
			auto pBuffer = new(std::nothrow) ConstantBuffer();
			if(!pBuffer)
			{
				ELOG("Error: Failed to allocate memory. \n");
				return false;
			}

			if(!pBuffer->Initialize(this->pDevice, pPool, BufferSize))
			{
				ELOG("Error: Failed to ConstantBuffer::Initialize()");
				return false;
			}

			SubsetList[i].pConstantBuffer = pBuffer;
			for(auto j = 0; j < TEXTURE_USAGE_COUNT; ++j)
			{
				SubsetList[i].TextureHandle[j].ptr = 0;
			}
		}
	}
	else
	{
		for(size_t i = 0; i < SubsetList.size(); ++i)
		{
			SubsetList[i].pConstantBuffer = nullptr;
			for(auto j = 0; j < TEXTURE_USAGE_COUNT; ++j)
			{
				SubsetList[i].TextureHandle[j].ptr = 0;
			}
		}	
	}

	return true;
}

void Material::Terminate()
{
	for(auto& Itr : this->TextureMap)
	{
		if(Itr.second)
		{
			Itr.second->Terminate();
			delete Itr.second;
			Itr.second = nullptr;
		}
	}

	for(size_t i = 0; i < SubsetList.size(); ++i)
	{
		if(!SubsetList[i].pConstantBuffer)
		{
			SubsetList[i].pConstantBuffer->Terminate();
			delete SubsetList[i].pConstantBuffer;
			SubsetList[i].pConstantBuffer = nullptr;
		}
	}

	TextureMap.clear();
	SubsetList.clear();

	if(this->pDevice)
	{
		this->pDevice->Release();
		this->pDevice = nullptr;
	}

	if(this->pPool)
	{
		this->pPool->Release();
		this->pPool = nullptr;
	}
}

const bool Material::SetTexture(const size_t MaterialNum, TEXTURE_USAGE TexUsage, const std::wstring& TexPath, DirectX::ResourceUploadBatch& Batch)
{
	// Validate range
	if(MaterialNum >= GetMaterialCount())
	{
		return false;
	}

	// Check RegisterdMaterial
	if(TextureMap.find(TexPath) != TextureMap.end())
	{
		SubsetList[MaterialNum].TextureHandle[TexUsage] = TextureMap[TexPath]->GetGPUHandle();
		return true;
	}

	// Check File Exist
	std::wstring FindPath = {};
	if(SearchFilePathW(TexPath.c_str(), FindPath))
	{
		SubsetList[MaterialNum].TextureHandle[TexUsage] = TextureMap[DummyTag]->GetGPUHandle();
		return true;
	}

	// Check weather fileName
	{
		if(PathIsDirectoryW(FindPath.c_str()) != FALSE)
		{
			SubsetList[MaterialNum].TextureHandle[TexUsage] = TextureMap[DummyTag]->GetGPUHandle();
			return true;
		}
	}

	// Generate Instance
	auto pTexture = new(std::nothrow) Texture();
	if(pTexture)
	{
		ELOG("Error: Failed to allocate memory. \n");
		return false;
	}

	// Initialize
	if(pTexture->Initialize(this->pDevice, this->pPool, FindPath.c_str(), Batch))
	{
		ELOG("Error: Failed to Texture::Initialize(). \n");
		pTexture->Terminate();
		delete pTexture;
		return false;
	}

	// Register
	TextureMap[TexPath] = pTexture;
	SubsetList[MaterialNum].TextureHandle[TexUsage] = pTexture->GetGPUHandle();

	// Success
	return true;
}

void* Material::GetBufferPtr(const size_t MaterialNum) const
{
	// Out of MaterialList
	if(MaterialNum >= GetMaterialCount())
	{
		return nullptr;
	}

	return SubsetList[MaterialNum].pConstantBuffer->GetPtr();
}

D3D12_GPU_VIRTUAL_ADDRESS Material::GetBufferAddress(const size_t MaterialNum) const
{
	// Out of MaterialList
	if(MaterialNum >= GetMaterialCount())
	{
		return D3D12_GPU_VIRTUAL_ADDRESS();
	}

	return SubsetList[MaterialNum].pConstantBuffer->GetAddress();
}

D3D12_GPU_DESCRIPTOR_HANDLE Material::GetTextureHandle(size_t MaterialNum, TEXTURE_USAGE TexUsage) const
{
	if(MaterialNum >= GetMaterialCount())
	{
		return D3D12_GPU_DESCRIPTOR_HANDLE();
	}

	return SubsetList[MaterialNum].TextureHandle[TexUsage];
}

const size_t Material::GetMaterialCount() const
{
	return SubsetList.size();
}

