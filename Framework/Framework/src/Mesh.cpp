#include "Mesh.h"
#include "Logger.h"

Mesh::Mesh()
	: MaterialID(UINT32_MAX)
	, IndexCount(0){
}

Mesh::~Mesh()
{
	Terminate();
}

const bool Mesh::Initialize(ID3D12Device* pDevice, const ResourceMesh& Resource)
{
	if(!pDevice)
	{
		ELOG("Error: Invalid argment. \n");
		return false;
	}

	// VertexBuffer
	if(!VertexBuffer_.Initialize(pDevice, sizeof(MeshVertex) * Resource.Vertices.size(),
		Resource.Vertices.data()))
	{
		ELOG("Error: Failed to Initialize VertexBuffer. \n");
		return false;
	}

	// IndexBuffer
	if(!IndexBuffer_.Initialize(pDevice, sizeof(uint32_t) * Resource.Indices.size(),
		Resource.Indices.data()))
	{
		ELOG("Error: Failed to Initialize IndexBuffer. \n");
		return false;
	}

	MaterialID = Resource.MaterialID;
	IndexCount = uint32_t(Resource.Indices.size());

	return true;
}

void Mesh::Terminate()
{
	VertexBuffer_.Terminate();
	IndexBuffer_.Terminate();
	MaterialID = UINT32_MAX;
	IndexCount = 0;
}

void Mesh::Draw(ID3D12GraphicsCommandList* pCmdList)
{
	auto VBView = VertexBuffer_.GetView();
	auto IBView = IndexBuffer_.GetView();

	pCmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pCmdList->IASetVertexBuffers(0, 1, &VBView);
	pCmdList->IASetIndexBuffer(&IBView);
	pCmdList->DrawIndexedInstanced(IndexCount, 1, 0, 0, 0);

}

const uint32_t Mesh::GetMaterialID() const
{
	return MaterialID;
}
