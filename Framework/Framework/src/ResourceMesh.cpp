#include "ResourceMesh.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/cimport.h>
#include <codecvt>
#include <cassert>

namespace {
	// convert 2 UTF-8
	std::string ToUTF8(const std::wstring& Value)
	{
		auto Length_ = WideCharToMultiByte(
			CP_UTF8, 0U, Value.data(), -1, nullptr, 0, nullptr, nullptr
		);	

		auto Buffer = new char[Length_];

		WideCharToMultiByte(
			CP_UTF8, 0U, Value.data(), -1, Buffer, Length_, nullptr, nullptr
		);

		std::string Result(Buffer);
		delete[] Buffer;
		Buffer = nullptr;

		return Result;
	}

	// convert 2 std::wstring
	std::wstring Convert(const aiString& Path)
	{
		wchar_t Temp[256] = {};
		size_t Size = -1;
		mbstowcs_s(&Size, Temp, Path.C_Str(), 256);

		return std::wstring(Temp);
	}

	// MeshLoader
class MeshLoader
{
public:
    MeshLoader();
    ~MeshLoader();

    const bool Load(const wchar_t* FileName, std::vector<ResourceMesh>& Meshes, std::vector<ResourceMaterial>& Materials);

private:
    void ParseMesh(ResourceMesh& DistMesh, const aiMesh* pSrcMesh);
    void ParseMaterial(ResourceMaterial& DistMaterial, const aiMaterial* pSrcMaterial);
};

MeshLoader::MeshLoader()
{

}

MeshLoader::~MeshLoader()
{

}

const bool MeshLoader::Load(const wchar_t* FileName, std::vector<ResourceMesh>& Meshes, std::vector<ResourceMaterial>& Materials)
{
    if (!FileName)
    {
        OutputDebugStringW(L"MeshLoader: Not find fileName. \n");
        return false;
    }

    // Load file
    auto Path = ToUTF8(FileName);

    Assimp::Importer Importer = {};
    int Flag = 0;
    Flag |= aiProcess_Triangulate;
    Flag |= aiProcess_PreTransformVertices;
    Flag |= aiProcess_CalcTangentSpace;
    Flag |= aiProcess_GenSmoothNormals;
    Flag |= aiProcess_GenUVCoords;
    Flag |= aiProcess_RemoveRedundantMaterials;
    Flag |= aiProcess_OptimizeMeshes;

    auto pScene = Importer.ReadFile(Path, Flag);

    if (!pScene)
    {
        OutputDebugStringW(L"MeshLoader: Not exist Scene. \n");
        return false;
    }

    // Reserve memory for Meshes
    Meshes.clear();
    Meshes.resize(pScene->mNumMeshes);

    // Transform MeshData
    for (size_t MeshCount = 0; MeshCount < Meshes.size(); ++MeshCount)
    {
        const auto pMesh = pScene->mMeshes[MeshCount];
        ParseMesh(Meshes[MeshCount], pMesh);
    }

    // Reserve memory for Materials
    Materials.clear();
    Materials.resize(pScene->mNumMaterials);

    // Transform MaterialData
    for (size_t MaterialCount = 0; MaterialCount < Materials.size(); ++MaterialCount)
    {
        const auto pMaterial = pScene->mMaterials[MaterialCount];
        ParseMaterial(Materials[MaterialCount], pMaterial);
    }

    pScene = nullptr;

    return true;
}

void MeshLoader::ParseMesh(ResourceMesh& DistMesh, const aiMesh* pSrcMesh)
{
    // Set MaterialNum
    DistMesh.MaterialID = pSrcMesh->mMaterialIndex;

    aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

    // Reserve memory for VertexData
    DistMesh.Vertices.resize(pSrcMesh->mNumVertices);

    for (auto VertexCount = 0u; VertexCount < pSrcMesh->mNumVertices; ++VertexCount)
    {
        auto pPosition = &(pSrcMesh->mVertices[VertexCount]);
        auto pNormal = &(pSrcMesh->mNormals[VertexCount]);
        auto pTexCoord = (pSrcMesh->HasTextureCoords(0)) ? &(pSrcMesh->mTextureCoords[0][VertexCount]) : &Zero3D;
        auto pTangent = (pSrcMesh->HasTangentsAndBitangents()) ? &(pSrcMesh->mTangents[VertexCount]) : &Zero3D;

        DistMesh.Vertices[VertexCount] = MeshVertex(
            DirectX::XMFLOAT3(pPosition->x, pPosition->y, pPosition->z),
            DirectX::XMFLOAT3(pNormal->x, pNormal->y, pNormal->z),
            DirectX::XMFLOAT2(pTexCoord->x, pTexCoord->y),
            DirectX::XMFLOAT3(pTangent->x, pTangent->y, pTangent->z)
        );
    }

    // Reserve memory for Index
    DistMesh.Indices.resize(pSrcMesh->mNumFaces * 3);

    for (auto IndexCount = 0u; IndexCount < pSrcMesh->mNumFaces; ++IndexCount)
    {
        const auto& Face = pSrcMesh->mFaces[IndexCount];
        assert(Face.mNumIndices == 3);

        DistMesh.Indices[IndexCount * 3 + 0] = Face.mIndices[0];
        DistMesh.Indices[IndexCount * 3 + 1] = Face.mIndices[1];
        DistMesh.Indices[IndexCount * 3 + 2] = Face.mIndices[2];
    }
}

void MeshLoader::ParseMaterial(ResourceMaterial& DistMaterial, const aiMaterial* pSrcMaterial)
{
    if (!pSrcMaterial)
    {
        OutputDebugStringW(L"Material is nullptr.\n");
        return;
    }

    // Diffuse
    {
        aiColor3D Color(0.0f, 0.0f, 0.0f);

        if (pSrcMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, Color) == AI_SUCCESS)
        {
            DistMaterial.Diffuse.x = Color.r;
            DistMaterial.Diffuse.y = Color.g;
            DistMaterial.Diffuse.z = Color.b;
        }
        else
        {
            DistMaterial.Diffuse.x = 1.0f;
            DistMaterial.Diffuse.y = 0.5f;
            DistMaterial.Diffuse.z = 0.5f;
        }
    }

    // Specular
    {
        aiColor3D Specular(0.0f, 0.0f, 0.0f);
        if (pSrcMaterial->Get(AI_MATKEY_COLOR_SPECULAR, Specular) == AI_SUCCESS)
        {
            DistMaterial.Specular.x = Specular.r;
            DistMaterial.Specular.y = Specular.g;
            DistMaterial.Specular.z = Specular.b;
        }
        else
        {
            DistMaterial.Specular.x = 0.0f;
            DistMaterial.Specular.y = 0.0f;
            DistMaterial.Specular.z = 0.0f;
        }
    }

    // Shiness
    {
        auto Shiness = 0.0f;
        if (pSrcMaterial->Get(AI_MATKEY_SHININESS, Shiness) == AI_SUCCESS)
        {
            DistMaterial.Shininess = Shiness;
        }
        else
        {
            DistMaterial.Shininess = 0.0f;
        }
    }

    // DiffuseMap
    {
        aiString Path = {};
        if (pSrcMaterial->Get(AI_MATKEY_TEXBLEND_DIFFUSE(0), Path) == AI_SUCCESS)
        {
            DistMaterial.DiffuseMap = Convert(Path);
        }
        else
        {
            DistMaterial.DiffuseMap.clear();
        }
    }
}

} // end of namespace

// Constant Values
#define FMT_FLOAT3 DXGI_FORMAT_R32G32B32_FLOAT
#define FMT_FLOAT2 DXGI_FORMAT_R32G32_FLOAT
#define APPEND D3D12_APPEND_ALIGNED_ELEMENT
#define IL_VERTEX D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA

const D3D12_INPUT_ELEMENT_DESC MeshVertex::InputElements[] = {
    { "POSITION", 0, FMT_FLOAT3, 0, APPEND, IL_VERTEX, 0 },
    { "NORMAL",   0, FMT_FLOAT3, 0, APPEND, IL_VERTEX, 0 },
    { "TEXCOORD", 0, FMT_FLOAT2, 0, APPEND, IL_VERTEX, 0 },
    { "TANGENT",  0, FMT_FLOAT3, 0, APPEND, IL_VERTEX, 0 }
};

const D3D12_INPUT_LAYOUT_DESC MeshVertex::InputLayout = {
    MeshVertex::InputElements,
    MeshVertex::InputElementCount,
};

static_assert(sizeof(MeshVertex) == 44, "Vertex struct layout mismatch\n");

#undef FMT_FLOAT3
#undef FMT_FLOAT2
#undef APPEND
#undef IL_VERTEX

// Load Mesh
const bool LoadMesh(
    const wchar_t* FileName,
    std::vector<ResourceMesh>& Meshes,
    std::vector<ResourceMaterial>& Materials
)
{
    MeshLoader Loader = {};
    return Loader.Load(FileName, Meshes, Materials);
}