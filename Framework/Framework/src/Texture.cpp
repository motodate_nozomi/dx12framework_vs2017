#include <Texture.h>
#include <DDSTextureLoader.h>
#include <DescriptorPool.h>
#include <Logger.h>

Texture::Texture()
	: pTexture(nullptr)
	, pHandle(nullptr)
	, pPool(nullptr){
}

Texture::~Texture()
{
	Terminate();
}

const bool Texture::Initialize(ID3D12Device* pDevice,
	DescriptorPool* pPool,
	const wchar_t* FileName,
	DirectX::ResourceUploadBatch& Batch)
{
	if(!pDevice || !pPool || !FileName)
	{

		return false;
	}

	assert(!this->pPool);
	assert(!this->pHandle);

	this->pPool = pPool;
	this->pPool->AddReff();

	// Get DesHandle
	this->pHandle = this->pPool->AllocHandle();
	if(!pHandle)
	{
		return false;
	}

	// Generate Texture
	bool bIsCube = false;
	auto HR = DirectX::CreateDDSTextureFromFile(
		pDevice,
		Batch,
		FileName,
		pTexture.GetAddressOf(),
		true,
		0,
		nullptr,
		&bIsCube
	);

	if(FAILED(HR))
	{
		ELOG("Error: DirectX::CrateDDSTextureFromFile() Filed. FileName = %ls \n", FileName);
		return false;
	}

	// Require SRV
	auto ViewDesc = GetViewDesc(bIsCube);

	// Generate SRV
	pDevice->CreateShaderResourceView(pTexture.Get(), &ViewDesc, this->pHandle->CPUHandle);

	// Success
	return true;
}

const bool Texture::Initialize(ID3D12Device* pDevice,
	DescriptorPool* pPool,
	const D3D12_RESOURCE_DESC* pDesc,
	const bool bIsCube)
{
	if(!pDevice || !pPool || !pDevice)
	{
		ELOG("Error: Initialize. Contains nullArg. \n");
		return false;
	}

	D3D12_HEAP_PROPERTIES Prop = {};
	Prop.Type = D3D12_HEAP_TYPE_DEFAULT;
	Prop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	Prop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	Prop.CreationNodeMask = 0;
	Prop.VisibleNodeMask = 0;

	auto HR = pDevice->CreateCommittedResource(
		&Prop,
		D3D12_HEAP_FLAG_NONE,
		pDesc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		nullptr,
		IID_PPV_ARGS(pTexture.GetAddressOf())
	);

	if(FAILED(HR))
	{
		ELOG("Error: DirectX::CrateDDSTextureFromFile() Filed. Retcode = 0x%x \n", HR);
		return false;
	}

	// Request SRV
	auto ViewDesc = GetViewDesc(bIsCube);

	// Generate SRV
	pDevice->CreateShaderResourceView(pTexture.Get(), &ViewDesc, pHandle->CPUHandle);

	// Success
	return true;
}

void Texture::Terminate()
{
	pTexture.Reset();

	if(!pHandle && !pPool)
	{
		pPool->FreeHandle(pHandle);
		pHandle = nullptr;
	}

	if(!pPool)
	{
		pPool->Release();
		pHandle = nullptr;
	}
}

D3D12_CPU_DESCRIPTOR_HANDLE Texture::GetCPUHandle() const
{
	if(!pHandle)
	{
		return pHandle->CPUHandle;
	}

	return D3D12_CPU_DESCRIPTOR_HANDLE();
}

D3D12_GPU_DESCRIPTOR_HANDLE Texture::GetGPUHandle() const
{
	if(!pHandle)
	{
		return pHandle->GPUHandle;
	}

	return D3D12_GPU_DESCRIPTOR_HANDLE();
}

D3D12_SHADER_RESOURCE_VIEW_DESC Texture::GetViewDesc(const bool bIsCube)
{
	auto Desc = pTexture->GetDesc();
	D3D12_SHADER_RESOURCE_VIEW_DESC ViewDesc = {};

	ViewDesc.Format = Desc.Format;
	ViewDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

	switch(Desc.Dimension)
	{
		// Exclude Buffer
		case D3D12_RESOURCE_DIMENSION_BUFFER:
		{
			abort();
			break;
		}

		case D3D12_RESOURCE_DIMENSION_TEXTURE2D:
		{
			// CubeMap
			if(bIsCube)
			{
				if(Desc.DepthOrArraySize > 6 )
				{
					ViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBEARRAY;
					
					ViewDesc.TextureCubeArray.MostDetailedMip = 0;
					ViewDesc.TextureCubeArray.MipLevels = Desc.MipLevels;
					ViewDesc.TextureCubeArray.First2DArrayFace = 0;
					ViewDesc.TextureCubeArray.NumCubes = (Desc.DepthOrArraySize / 6);
					ViewDesc.TextureCubeArray.ResourceMinLODClamp = 0.0f;
				}
				else
				{
					ViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBE;

					ViewDesc.TextureCube.MostDetailedMip = 0;
					ViewDesc.TextureCube.MipLevels = Desc.MipLevels;
					ViewDesc.TextureCube.ResourceMinLODClamp = 0.0f;
				}
			}
			// No CubeMap
			else
			{
				if(Desc.DepthOrArraySize > 1)
				{
					if(Desc.MipLevels > 1)
					{
						ViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DMSARRAY;

						ViewDesc.Texture2DMSArray.FirstArraySlice = 0;
						ViewDesc.Texture2DMSArray.ArraySize = Desc.DepthOrArraySize;
					}
					else
					{
						ViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DARRAY;

						ViewDesc.Texture2DArray.MostDetailedMip = 0;
						ViewDesc.Texture2DArray.MipLevels = Desc.MipLevels;
						ViewDesc.Texture2DArray.FirstArraySlice = 0;
						ViewDesc.Texture2DArray.ArraySize = Desc.DepthOrArraySize;
						ViewDesc.Texture2DArray.PlaneSlice = 0;
						ViewDesc.Texture2DArray.ResourceMinLODClamp = 0.0f;				
					}

				}
				else
				{
					if(Desc.MipLevels > 1)
					{
						ViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DMS;
					}
					else
					{
						ViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;

						ViewDesc.Texture2D.MostDetailedMip = 0;
						ViewDesc.Texture2D.MipLevels = Desc.MipLevels;
						ViewDesc.Texture2D.PlaneSlice = 0;
						ViewDesc.Texture2D.ResourceMinLODClamp = 0.0f;
					}
				}
			}
			break;
		}

		case D3D12_RESOURCE_DIMENSION_TEXTURE3D:
		{
			ViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE3D;

			ViewDesc.Texture3D.MostDetailedMip = 0;
			ViewDesc.Texture3D.MipLevels = Desc.MipLevels;
			ViewDesc.Texture3D.ResourceMinLODClamp = 0.0f;
		}

		// Out of blue
		default:
		{
			abort();
			break;
		}
	}

	return ViewDesc;
}