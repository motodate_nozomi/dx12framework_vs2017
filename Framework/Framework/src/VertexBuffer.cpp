#include "VertexBuffer.h"
#include "Logger.h"

VertexBuffer::VertexBuffer()
	: pVertexBuffer(nullptr){
	memset(&VBView, 0, sizeof(VBView));
}

VertexBuffer::~VertexBuffer()
{
	Terminate();
}

const bool VertexBuffer::Initialize(ID3D12Device* pDevice, const size_t Size, const size_t Stride, const void* pInitData /*= nullptr*/)
{
	// Validate argument.
	if(!pDevice || Size <= 0 || Stride <= 0)
	{
		ELOG("Error: VertexBuffer.cpp_Invalid argument. \n");
		return false;
	}

	// Heap Prop
	D3D12_HEAP_PROPERTIES Prop = {};
	Prop.Type = D3D12_HEAP_TYPE_UPLOAD;
	Prop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	Prop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	Prop.CreationNodeMask = 1;
	Prop.VisibleNodeMask = 1;

	// Set Resource
	D3D12_RESOURCE_DESC Desc = {};
	Desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	Desc.Alignment = 0;
	Desc.Width = UINT64(Size);
	Desc.Height = 1;
	Desc.DepthOrArraySize = 1;
	Desc.MipLevels = 1;
	Desc.Format = DXGI_FORMAT_UNKNOWN;
	Desc.SampleDesc.Count = 1;
	Desc.SampleDesc.Quality = 0;
	Desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	Desc.Flags = D3D12_RESOURCE_FLAG_NONE;

	// Generate Resource
	auto HR = pDevice->CreateCommittedResource(
		&Prop,
		D3D12_HEAP_FLAG_NONE,
		&Desc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(pVertexBuffer.GetAddressOf())
	);
	if(FAILED(HR))
	{
		ELOG("Error: Failed generate resource. \n");
		return false;
	}

	// Set VertexVufferView
	VBView.BufferLocation = pVertexBuffer->GetGPUVirtualAddress();
	VBView.StrideInBytes = UINT(Stride);
	VBView.SizeInBytes = UINT(Size);

	// Write if exist InitData
	if(pInitData)
	{
		void* Ptr = Map();
		if(!Ptr)
		{
			ELOG("Error: Failed write InitData. \n");
			return false;
		}

		memcpy(Ptr, pInitData, Size);

		pVertexBuffer->Unmap(0, nullptr);
	}

	// Success
	return true;
}

void VertexBuffer::Terminate()
{
	pVertexBuffer.Reset();
	memset(&VBView, 0, sizeof(VBView));
}

void* VertexBuffer::Map()
{
	void* Ptr = nullptr;
	auto HR = pVertexBuffer->Map(0, nullptr, &Ptr);
	if(FAILED(HR))
	{
		return nullptr;
	}

	return Ptr;
}

void VertexBuffer::Unmap()
{
	pVertexBuffer->Unmap(0, nullptr);
}

D3D12_VERTEX_BUFFER_VIEW VertexBuffer::GetView() const
{
	return VBView;
}
